#include <iostream>

using namespace std;

void P()
{
    cout << " |----| " << endl;
    cout << " |    | " << endl;
    cout << " |----| " << endl;
    cout << " |      " << endl;
    cout << " |      " << endl;
    cout << " |      " << endl;
}

void l()
{
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
}

void a()
{
    cout << "        " << endl;
    cout << "        " << endl;
    cout << "   /\\   " << endl;
    cout << "  /  \\  " << endl;
    cout << " /____\\ " << endl;
    cout << "/      \\" << endl;
}

void t()
{
    cout << "   ||   " << endl;
    cout << " __||__ " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
    cout << "    \\__" << endl;
}

void o()
{
    cout << "   ____   " << endl;
    cout << "  //  \\   " << endl;
    cout << " //    \\  " << endl;
    cout << "||      ||  " << endl;
    cout << " \\     // " << endl;
    cout << "  \\___//  " << endl;
}

void n()
{
    cout << "        " << endl;
    cout << "||_____ " << endl;
    cout << "||    \\" << endl;
    cout << "||    ||" << endl;
    cout << "||    ||" << endl;
    cout << "||    ||" << endl;
}

int main()
{
    P();
    l();
    a();
    t();
    o();
    n();
    return 0;
}
